<?php
    define("ROOT", __DIR__ ."/");
?>
<?php

    include_once(ROOT ."View/doctype.php");
    include_once(ROOT ."View/head.php");
    include_once(ROOT ."View/body-open.php");
    include_once(ROOT ."View/main.php");
    include_once(ROOT ."View/body-close.php");
    include_once(ROOT ."View/footer.php");