/**
 * Function sendForm
 * 
 * Get Form Input value
 * Check if Input values are empty
 * If Not  ->  make fetch request to php controller
 * 
 * @param {event} e 
 */
async function sendForm(e) {
    e.preventDefault();
    const INPUT1_VALUE = parseInt(e.target.form.children[0].value);
    const INPUT2_VALUE = parseInt(e.target.form.children[1].value);
    const RESULT_ELEMENT = document.querySelector("#resultCalcul");
    
    if (INPUT1_VALUE && INPUT2_VALUE) {
        let response = await fetch(`./Controller/SommeController.php?q1=${INPUT1_VALUE}&q2=${INPUT2_VALUE}`, {
            method: 'GET'
        });

        if (response.ok) {
            let data = await response.text();
            RESULT_ELEMENT.innerHTML = data;
        }
    }
}

// Initialise an event listener how when form button was click call sendForm function
document.querySelector('form button[type=submit]').addEventListener('click', sendForm);