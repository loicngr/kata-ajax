<?php
    /**
     * If "ROOT" variable is not defined
     *      If is ajax request, "ROOT" variable is not defined because is define in index.php file
     */
    if (!defined("ROOT")) {
        define("ROOT", "../");
    }
    require(ROOT ."Model/SommeModel.php");

    /**
     * Check if GET array is not empty
     */
    if (!empty($_GET)) {
        // If q1 and q2 is definied
        if (isset($_GET['q1']) && isset($_GET['q2'])) {
            echo calculateSomme(intval($_GET['q1']), intval($_GET['q2']));
        }
    }

    /**
     * Simple calcul function how send data to Model
     */
    function calculateSomme( $a, $b) {
        return modelSomme($a, $b);
    }